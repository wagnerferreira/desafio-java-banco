package com.southsystem.desafiojavabanco.repository;

import com.southsystem.desafiojavabanco.entity.ContaCorrente;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContaCorrenteRepository extends CrudRepository<ContaCorrente, Long> {

}