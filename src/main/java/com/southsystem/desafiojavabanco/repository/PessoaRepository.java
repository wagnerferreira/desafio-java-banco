package com.southsystem.desafiojavabanco.repository;

import com.southsystem.desafiojavabanco.entity.Pessoa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PessoaRepository extends CrudRepository<Pessoa, Long> {

}