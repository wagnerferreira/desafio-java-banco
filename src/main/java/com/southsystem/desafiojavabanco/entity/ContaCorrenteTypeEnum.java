package com.southsystem.desafiojavabanco.entity;

public enum ContaCorrenteTypeEnum {

    CORRENTE('C'), EMPRESARIAL('E');

    private final Character code;

    ContaCorrenteTypeEnum(Character code) {
        this.code = code;
    }

    public Character getCode() {
        return this.code;
    }

}