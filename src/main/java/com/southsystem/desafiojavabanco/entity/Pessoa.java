package com.southsystem.desafiojavabanco.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "CPF ou CNPJ é obrigatório")
    @NotNull(message = "CPF ou CNPJ é obrigatório")
    @Length(max = 14, min = 11)
    private String cpfCnpj;

    private PessoaTypeEnum tipo;

    @Max(9)
    private int score;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public PessoaTypeEnum getTipo() {
        return tipo;
    }

    public void setTipo(PessoaTypeEnum tipo) {
        this.tipo = tipo;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

}