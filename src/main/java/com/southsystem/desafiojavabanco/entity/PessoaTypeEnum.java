package com.southsystem.desafiojavabanco.entity;

public enum PessoaTypeEnum {

    FISICA("PF"), JURIDICA("PJ");

    private final String code;

    PessoaTypeEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

}