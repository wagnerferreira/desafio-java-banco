package com.southsystem.desafiojavabanco.entity;

import java.util.Arrays;
import java.util.List;

public enum LimiteEnum {

    NONE(0d, 0d, Arrays.asList(0, 1)), 
    SILVER(1000d, 200d, Arrays.asList(2, 3, 4, 5)),
    GOLD(2000d, 2000d, Arrays.asList(6, 7, 8)), 
    PLATINUM(5000d, 15000d, Arrays.asList(9));

    private final Double limiteCartaoCredito;
    private final Double limiteChequeEspecial;
    private final List<Integer> rangeScore;

    LimiteEnum(Double limiteChequeEspecial, Double limiteCartaoCredito, List<Integer> rangeScore) {
        this.limiteCartaoCredito = limiteCartaoCredito;
        this.limiteChequeEspecial = limiteChequeEspecial;
        this.rangeScore = rangeScore;
    }

    public List<Integer> getRangeScore() {
        return this.rangeScore;
    }

    public Double getLimiteCartaoCredito() {
        return this.limiteCartaoCredito;
    }

    public Double getLimiteChequeEspecial() {
        return this.limiteChequeEspecial;
    }

}