package com.southsystem.desafiojavabanco.controller;

import java.util.List;

import com.southsystem.desafiojavabanco.entity.Pessoa;
import com.southsystem.desafiojavabanco.service.PessoaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @PostMapping(value = "/pessoa")
    public ResponseEntity<Pessoa> save(@RequestBody Pessoa pessoa) {
        return new ResponseEntity<>(pessoaService.save(pessoa), HttpStatus.OK);
    }

    @GetMapping("/pessoa")
    public ResponseEntity<List<Pessoa>> findAll() {
        return new ResponseEntity<>((List<Pessoa>) pessoaService.findAll(), HttpStatus.OK);
    }

}