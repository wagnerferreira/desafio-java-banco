package com.southsystem.desafiojavabanco.controller;

import java.util.List;

import com.southsystem.desafiojavabanco.entity.ContaCorrente;
import com.southsystem.desafiojavabanco.service.ContaCorrenteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class ContaCorrenteController {

    @Autowired
    private ContaCorrenteService contaCorrenteService;

    @GetMapping("/conta-corrente")
    public ResponseEntity<List<ContaCorrente>> findAll() {
        return new ResponseEntity<>(contaCorrenteService.findAll(), HttpStatus.OK);
    }

}