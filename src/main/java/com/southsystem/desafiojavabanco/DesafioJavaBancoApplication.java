package com.southsystem.desafiojavabanco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioJavaBancoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioJavaBancoApplication.class, args);
	}

}
