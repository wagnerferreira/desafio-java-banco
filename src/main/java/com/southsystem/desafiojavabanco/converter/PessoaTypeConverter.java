package com.southsystem.desafiojavabanco.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.southsystem.desafiojavabanco.entity.PessoaTypeEnum;

@Converter(autoApply = true)
public class PessoaTypeConverter implements AttributeConverter<PessoaTypeEnum, String> {
  
    @Override
    public String convertToDatabaseColumn(PessoaTypeEnum pessoaTypeEnum) {
        if (pessoaTypeEnum == null) {
            return null;
        }
        return pessoaTypeEnum.getCode();
    }
 
    @Override
    public PessoaTypeEnum convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
 
        return Stream.of(PessoaTypeEnum.values())
          .filter(c -> c.getCode().equals(code)).findFirst()
          .orElseThrow(IllegalArgumentException::new);
    }
}