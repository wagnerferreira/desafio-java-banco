package com.southsystem.desafiojavabanco.converter;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.southsystem.desafiojavabanco.entity.ContaCorrenteTypeEnum;

@Converter(autoApply = true)
public class ContaCorrenteTypeConverter implements AttributeConverter<ContaCorrenteTypeEnum, Character> {
  
    @Override
    public Character convertToDatabaseColumn(ContaCorrenteTypeEnum contaCorrenteTypeEnum) {
        if (contaCorrenteTypeEnum == null) {
            return null;
        }
        return contaCorrenteTypeEnum.getCode();
    }
 
    @Override
    public ContaCorrenteTypeEnum convertToEntityAttribute(Character code) {
        if (code == null) {
            return null;
        }
 
        return Stream.of(ContaCorrenteTypeEnum.values())
          .filter(c -> c.getCode().equals(code)).findFirst()
          .orElseThrow(IllegalArgumentException::new);
    }
}