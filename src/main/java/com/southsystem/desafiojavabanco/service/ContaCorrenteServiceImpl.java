package com.southsystem.desafiojavabanco.service;

import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Stream;

import com.southsystem.desafiojavabanco.entity.ContaCorrente;
import com.southsystem.desafiojavabanco.entity.ContaCorrenteTypeEnum;
import com.southsystem.desafiojavabanco.entity.LimiteEnum;
import com.southsystem.desafiojavabanco.entity.Pessoa;
import com.southsystem.desafiojavabanco.entity.PessoaTypeEnum;
import com.southsystem.desafiojavabanco.repository.ContaCorrenteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ContaCorrenteServiceImpl implements ContaCorrenteService {

    @Value("${agencia.numero}")
    private String agencia;

    @Autowired
    private ContaCorrenteRepository contaCorrenteRepository;

    @Override
    public ContaCorrente abrirContaCorrente(Pessoa pessoa) {
        if (Objects.isNull(pessoa)) {
            throw new NullPointerException("É obrigatório existir uma Pessoa para gerar uma conta corrente.");
        }

        ContaCorrente contaCorrente = new ContaCorrente();
        contaCorrente.setPessoa(pessoa);
        contaCorrente.setAgencia(agencia);
        contaCorrente.setNumero(gerarNumeroConta(6));

        if (pessoa.getTipo().equals(PessoaTypeEnum.FISICA)) {
            contaCorrente.setTipo(ContaCorrenteTypeEnum.CORRENTE);
        } else {
            contaCorrente.setTipo(ContaCorrenteTypeEnum.EMPRESARIAL);
        }

        contaCorrenteRepository.save(contaCorrente);
        gerarLimites(contaCorrente);

        return contaCorrente;
    }

    @Override
    public List<ContaCorrente> findAll() {
        return (List<ContaCorrente>) contaCorrenteRepository.findAll();
    }

    private int gerarNumeroConta(int n) {
        int m = (int) Math.pow(10, n - 1);
        return m + new Random().nextInt(9 * m);
    }

    private void gerarLimites(ContaCorrente contaCorrente) {
        int score = contaCorrente.getPessoa().getScore();

        LimiteEnum limiteEnum = Stream.of(LimiteEnum.values()).filter(c -> c.getRangeScore().contains(score))
                .findFirst().orElseThrow(IllegalArgumentException::new);

        contaCorrente.setLimiteCartaoCredito(limiteEnum.getLimiteCartaoCredito());
        contaCorrente.setLimiteChequeEspecial(limiteEnum.getLimiteChequeEspecial());
        contaCorrenteRepository.save(contaCorrente);
    }

}