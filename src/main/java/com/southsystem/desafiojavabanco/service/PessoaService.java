package com.southsystem.desafiojavabanco.service;

import java.util.List;

import com.southsystem.desafiojavabanco.entity.Pessoa;

public interface PessoaService {

    Pessoa save(Pessoa pessoa);

    List<Pessoa> findAll();

}