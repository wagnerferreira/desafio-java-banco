package com.southsystem.desafiojavabanco.service;

import java.util.List;
import java.util.Random;

import com.southsystem.desafiojavabanco.entity.Pessoa;
import com.southsystem.desafiojavabanco.entity.PessoaTypeEnum;
import com.southsystem.desafiojavabanco.repository.PessoaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PessoaServiceImpl implements PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private ContaCorrenteService contaCorrenteService;

    @Override
    public Pessoa save(Pessoa pessoa) {
        if (pessoa.getCpfCnpj().length() < 11
                || (pessoa.getCpfCnpj().length() > 11 && pessoa.getCpfCnpj().length() < 14)
                || pessoa.getCpfCnpj().length() > 14) {
            throw new IllegalArgumentException("CPF ou CNPJ deve ter o tamanho de 11 ou 14 digitos.");
        }

        if (pessoa.getCpfCnpj().length() == 11) {
            pessoa.setTipo(PessoaTypeEnum.FISICA);
        } else if (pessoa.getCpfCnpj().length() == 14) {
            pessoa.setTipo(PessoaTypeEnum.JURIDICA);
        }

        Random random = new Random();
        pessoa.setScore(random.nextInt(10));
        pessoa = pessoaRepository.save(pessoa);

        contaCorrenteService.abrirContaCorrente(pessoa);

        return pessoa;
    }

    @Override
    public List<Pessoa> findAll() {
        return (List<Pessoa>) pessoaRepository.findAll();
    }

}