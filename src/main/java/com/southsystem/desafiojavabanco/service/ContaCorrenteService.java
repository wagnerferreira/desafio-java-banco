package com.southsystem.desafiojavabanco.service;

import java.util.List;

import com.southsystem.desafiojavabanco.entity.ContaCorrente;
import com.southsystem.desafiojavabanco.entity.Pessoa;

public interface ContaCorrenteService {

    ContaCorrente abrirContaCorrente(Pessoa pessoa);

    List<ContaCorrente> findAll();

}