package com.southsystem.desafiojavabanco;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.southsystem.desafiojavabanco.entity.Pessoa;
import com.southsystem.desafiojavabanco.entity.PessoaTypeEnum;
import com.southsystem.desafiojavabanco.repository.PessoaRepository;
import com.southsystem.desafiojavabanco.service.ContaCorrenteService;
import com.southsystem.desafiojavabanco.service.PessoaService;
import com.southsystem.desafiojavabanco.service.PessoaServiceImpl;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PessoaServiceTest {

    @Mock
    private PessoaRepository pessoaRepository;

    @Mock
    private ContaCorrenteService contaCorrenteService;

    @InjectMocks
    private PessoaService pessoaService = new PessoaServiceImpl();
    
    @Test
    public void save_setTypePessoaFisica() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("01511628083");
        
        when(pessoaRepository.save(pessoa)).thenReturn(pessoa);
        when(contaCorrenteService.abrirContaCorrente(any(Pessoa.class))).thenReturn(any());

        pessoa = pessoaService.save(pessoa);

        assertEquals(PessoaTypeEnum.FISICA, pessoa.getTipo());
    }

    @Test
    public void save_setTypePessoaJuridica() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("25543044000199");
        
        when(pessoaRepository.save(pessoa)).thenReturn(pessoa);
        when(contaCorrenteService.abrirContaCorrente(any(Pessoa.class))).thenReturn(any());
        pessoa = pessoaService.save(pessoa);

        assertEquals(PessoaTypeEnum.JURIDICA, pessoa.getTipo());
    }

    @Test
    public void save_setTypePessoaJuridicaWithContaCorrente() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("25543044000199");
        
        when(pessoaRepository.save(pessoa)).thenReturn(pessoa);
        when(contaCorrenteService.abrirContaCorrente(any(Pessoa.class))).thenReturn(any());
        pessoa = pessoaService.save(pessoa);

        assertEquals(PessoaTypeEnum.JURIDICA, pessoa.getTipo());
    }

    @Test
    public void save_cpfMenor11() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("0151162808");

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            pessoaService.save(pessoa);
        });
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains("CPF ou CNPJ deve ter o tamanho de 11 ou 14 digitos."));
    }

    @Test
    public void save_cnpjMenor14() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("0151162808223");

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            pessoaService.save(pessoa);
        });
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains("CPF ou CNPJ deve ter o tamanho de 11 ou 14 digitos."));
    }

    @Test
    public void save_cnpjMaior14() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("0151162808223323232323");

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            pessoaService.save(pessoa);
        });
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains("CPF ou CNPJ deve ter o tamanho de 11 ou 14 digitos."));
    }
}