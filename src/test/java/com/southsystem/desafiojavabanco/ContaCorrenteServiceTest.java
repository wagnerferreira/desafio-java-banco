package com.southsystem.desafiojavabanco;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.southsystem.desafiojavabanco.entity.ContaCorrente;
import com.southsystem.desafiojavabanco.entity.ContaCorrenteTypeEnum;
import com.southsystem.desafiojavabanco.entity.LimiteEnum;
import com.southsystem.desafiojavabanco.entity.Pessoa;
import com.southsystem.desafiojavabanco.entity.PessoaTypeEnum;
import com.southsystem.desafiojavabanco.repository.ContaCorrenteRepository;
import com.southsystem.desafiojavabanco.service.ContaCorrenteService;
import com.southsystem.desafiojavabanco.service.ContaCorrenteServiceImpl;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ContaCorrenteServiceTest {

    @Mock
    private ContaCorrenteRepository contaCorrenteRepository;

    @InjectMocks
    private ContaCorrenteService pessoaService = new ContaCorrenteServiceImpl();

    @Test
    public void gerarLimites_contaCorrenteIsNull() {
        Exception exception = assertThrows(NullPointerException.class, () -> {
            pessoaService.abrirContaCorrente(null);
        });
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains("É obrigatório existir uma Pessoa para gerar uma conta corrente."));
    }

    @Test
    public void gerarLimites_contaTipoCorrenteScore1() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("01511615848");
        pessoa.setScore(1);
        pessoa.setTipo(PessoaTypeEnum.FISICA);

        when(contaCorrenteRepository.save(any())).thenReturn(any());

        ContaCorrente contaCorrente = pessoaService.abrirContaCorrente(pessoa);

        assertEquals(ContaCorrenteTypeEnum.CORRENTE, contaCorrente.getTipo());
        assertEquals(LimiteEnum.NONE.getLimiteCartaoCredito(), contaCorrente.getLimiteCartaoCredito());
        assertEquals(LimiteEnum.NONE.getLimiteChequeEspecial(), contaCorrente.getLimiteChequeEspecial());
    }
    
    @Test
    public void gerarLimites_contaTipoCorrenteScore2() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("01511615848");
        pessoa.setScore(2);
        pessoa.setTipo(PessoaTypeEnum.FISICA);

        when(contaCorrenteRepository.save(any())).thenReturn(any());

        ContaCorrente contaCorrente = pessoaService.abrirContaCorrente(pessoa);

        assertEquals(ContaCorrenteTypeEnum.CORRENTE, contaCorrente.getTipo());
        assertEquals(LimiteEnum.SILVER.getLimiteCartaoCredito(), contaCorrente.getLimiteCartaoCredito());
        assertEquals(LimiteEnum.SILVER.getLimiteChequeEspecial(), contaCorrente.getLimiteChequeEspecial());
    }

    @Test
    public void gerarLimites_contaTipoCorrenteScore4() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("01511615848");
        pessoa.setScore(4);
        pessoa.setTipo(PessoaTypeEnum.FISICA);

        when(contaCorrenteRepository.save(any())).thenReturn(any());

        ContaCorrente contaCorrente = pessoaService.abrirContaCorrente(pessoa);

        assertEquals(ContaCorrenteTypeEnum.CORRENTE, contaCorrente.getTipo());
        assertEquals(LimiteEnum.SILVER.getLimiteCartaoCredito(), contaCorrente.getLimiteCartaoCredito());
        assertEquals(LimiteEnum.SILVER.getLimiteChequeEspecial(), contaCorrente.getLimiteChequeEspecial());
    }

    @Test
    public void gerarLimites_contaTipoCorrenteScore6() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("01511615848");
        pessoa.setScore(6);
        pessoa.setTipo(PessoaTypeEnum.FISICA);

        when(contaCorrenteRepository.save(any())).thenReturn(any());

        ContaCorrente contaCorrente = pessoaService.abrirContaCorrente(pessoa);

        assertEquals(ContaCorrenteTypeEnum.CORRENTE, contaCorrente.getTipo());
        assertEquals(LimiteEnum.GOLD.getLimiteCartaoCredito(), contaCorrente.getLimiteCartaoCredito());
        assertEquals(LimiteEnum.GOLD.getLimiteChequeEspecial(), contaCorrente.getLimiteChequeEspecial());
    }

    @Test
    public void gerarLimites_contaTipoCorrenteScore8() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("01511615848");
        pessoa.setScore(8);
        pessoa.setTipo(PessoaTypeEnum.FISICA);

        when(contaCorrenteRepository.save(any())).thenReturn(any());

        ContaCorrente contaCorrente = pessoaService.abrirContaCorrente(pessoa);

        assertEquals(ContaCorrenteTypeEnum.CORRENTE, contaCorrente.getTipo());
        assertEquals(LimiteEnum.GOLD.getLimiteCartaoCredito(), contaCorrente.getLimiteCartaoCredito());
        assertEquals(LimiteEnum.GOLD.getLimiteChequeEspecial(), contaCorrente.getLimiteChequeEspecial());
    }

    @Test
    public void gerarLimites_contaTipoEmpresarialScore9() {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpfCnpj("015135258818");
        pessoa.setScore(9);
        pessoa.setTipo(PessoaTypeEnum.JURIDICA);

        when(contaCorrenteRepository.save(any())).thenReturn(any());

        ContaCorrente contaCorrente = pessoaService.abrirContaCorrente(pessoa);

        assertEquals(ContaCorrenteTypeEnum.EMPRESARIAL, contaCorrente.getTipo());
        assertEquals(LimiteEnum.PLATINUM.getLimiteCartaoCredito(), contaCorrente.getLimiteCartaoCredito());
        assertEquals(LimiteEnum.PLATINUM.getLimiteChequeEspecial(), contaCorrente.getLimiteChequeEspecial());
    }
    
}